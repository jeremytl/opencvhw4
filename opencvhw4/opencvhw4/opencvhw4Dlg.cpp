
// opencvhw4Dlg.cpp : 實作檔
//

#include "stdafx.h"
#include "opencvhw4.h"
#include "opencvhw4Dlg.h"
#include "afxdialogex.h"
#include "opencv2\highgui\highgui.hpp"
#include "opencv\cv.h"
#include "stdio.h"

#include <vector>


using namespace cv;
using namespace std;


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 對 App About 使用 CAboutDlg 對話方塊

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 對話方塊資料
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支援

// 程式碼實作
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Copencvhw4Dlg 對話方塊



Copencvhw4Dlg::Copencvhw4Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(Copencvhw4Dlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void Copencvhw4Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(Copencvhw4Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &Copencvhw4Dlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON4, &Copencvhw4Dlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON2, &Copencvhw4Dlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &Copencvhw4Dlg::OnBnClickedButton3)
END_MESSAGE_MAP()


// Copencvhw4Dlg 訊息處理常式

BOOL Copencvhw4Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 將 [關於...] 功能表加入系統功能表。

	// IDM_ABOUTBOX 必須在系統命令範圍之中。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 設定此對話方塊的圖示。當應用程式的主視窗不是對話方塊時，
	// 框架會自動從事此作業
	SetIcon(m_hIcon, TRUE);			// 設定大圖示
	SetIcon(m_hIcon, FALSE);		// 設定小圖示

	// TODO:  在此加入額外的初始設定
	AllocConsole();
	freopen("CONOUT$", "w", stdout);

	return TRUE;  // 傳回 TRUE，除非您對控制項設定焦點
}

void Copencvhw4Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果將最小化按鈕加入您的對話方塊，您需要下列的程式碼，
// 以便繪製圖示。對於使用文件/檢視模式的 MFC 應用程式，
// 框架會自動完成此作業。

void Copencvhw4Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 繪製的裝置內容

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 將圖示置中於用戶端矩形
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 描繪圖示
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 當使用者拖曳最小化視窗時，
// 系統呼叫這個功能取得游標顯示。
HCURSOR Copencvhw4Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


//1-1
void Copencvhw4Dlg::OnBnClickedButton1()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CvCapture* src = cvCreateFileCapture("./HW_video.mp4");
	IplImage* frame = cvQueryFrame(src);
	cvNamedWindow("1-1", CV_WINDOW_AUTOSIZE);
	CvPoint2D32f *feature = new CvPoint2D32f[7];
	feature[0] = cvPoint2D32f(117.0f, 72.0f);
	feature[1] = cvPoint2D32f(111.0f, 95.0f);
	feature[2] = cvPoint2D32f(118.0f, 168.0f);
	feature[3] = cvPoint2D32f(135.0f, 240.0f);
	feature[4] = cvPoint2D32f(129.0f, 260.0f);
	feature[5] = cvPoint2D32f(175.0f, 269.0f);
	feature[6] = cvPoint2D32f(193.0f, 257.0f);
	CvSize win_sz = cvSize(10,10);
	
	FILE *a=fopen("hw3_1.txt","w");
	for (int i = 0; i < 7;i++)
	fprintf(a, "%d %d %d %d\n", (int)feature[i].x, (int)feature[i].y, win_sz.height, win_sz.width);

	fclose(a);

	for (int i = 0; i < 7; i++)
	{

		cvLine(frame, cvPoint(feature[i].x - 5, feature[i].y - 5), cvPoint(feature[i].x + 5, feature[i].y - 5), cvScalar(0x00, 0x00, 0xff));
		cvLine(frame, cvPoint(feature[i].x - 5, feature[i].y), cvPoint(feature[i].x + 5, feature[i].y), cvScalar(0x00, 0x00, 0xff));
		cvLine(frame, cvPoint(feature[i].x - 5, feature[i].y + 5), cvPoint(feature[i].x + 5, feature[i].y + 5), cvScalar(0x00, 0x00, 0xff));
		cvLine(frame, cvPoint(feature[i].x - 5, feature[i].y - 5), cvPoint(feature[i].x - 5, feature[i].y + 5), cvScalar(0x00, 0x00, 0xff));
		cvLine(frame, cvPoint(feature[i].x , feature[i].y - 5), cvPoint(feature[i].x , feature[i].y + 5), cvScalar(0x00, 0x00, 0xff));
		cvLine(frame, cvPoint(feature[i].x + 5, feature[i].y - 5), cvPoint(feature[i].x + 5, feature[i].y + 5), cvScalar(0x00, 0x00, 0xff));
	}
	//cvCircle(frame, cvPoint(193, 258), 3, cvScalar(0xff, 0x00, 0x00));
	cvShowImage("1-1", frame);
	cvWaitKey(0);
	cvReleaseCapture(&src);
	cvDestroyWindow("1-1");
}

//1-2
void Copencvhw4Dlg::OnBnClickedButton4()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CvCapture* src = cvCreateFileCapture("./HW_video.mp4");
	IplImage* frame = cvQueryFrame(src);
	IplImage* g_frame = cvCreateImage(cvGetSize(frame), IPL_DEPTH_8U, 1);
	cvCvtColor(frame, g_frame, CV_BGR2GRAY);
	//cvNamedWindow("1-1", CV_WINDOW_AUTOSIZE);
	CvPoint2D32f *feature = new CvPoint2D32f[7];
	CvPoint2D32f *feature2 = new CvPoint2D32f[7];
	feature[0] = cvPoint2D32f(117.0f, 72.0f);
	feature[1] = cvPoint2D32f(111.0f, 95.0f);
	feature[2] = cvPoint2D32f(118.0f, 168.0f);
	feature[3] = cvPoint2D32f(135.0f, 240.0f);
	feature[4] = cvPoint2D32f(129.0f, 260.0f);
	feature[5] = cvPoint2D32f(175.0f, 269.0f);
	feature[6] = cvPoint2D32f(193.0f, 257.0f);
	CvSize win_sz = cvSize(10,10);

	IplImage* cmp_frame = cvQueryFrame(src);
	IplImage* g_cmp_frame = cvCreateImage(cvGetSize(cmp_frame), IPL_DEPTH_8U, 1);
	cvCvtColor(cmp_frame, g_cmp_frame, CV_BGR2GRAY);

	char features_found[7];
	float feature_errors[7];
	CvSize pyr_sz = cvSize(frame->width + 8, cmp_frame->height / 3);
	IplImage* pyrA = cvCreateImage(pyr_sz, IPL_DEPTH_8U, 1);
	IplImage* pyrB = cvCreateImage(pyr_sz, IPL_DEPTH_8U, 1);

	FILE *a = fopen("hw3_2.txt", "w");

	int f_cnt;
	printf("no. of frame:");
	freopen("CONIN$", "r", stdin);
	scanf("%d", &f_cnt);
	for (int i = 0; i < f_cnt; i++)
	{
		cvCalcOpticalFlowPyrLK(
			g_frame,
			g_cmp_frame,
			pyrA,
			pyrB,
			feature,
			feature2,
			7,
			win_sz,
			5,
			features_found,
			feature_errors,
			cvTermCriteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 20, 0.3),
			0
			);

		fprintf(a, "frame%04d\n", i);
		for (int j = 0; j < 7; j++)
		{
			//fprintf(a, " %d", features_found[i]);
			if (features_found[j]!=0)
			{
				fprintf(a, "%f %f %d %d\n", feature2[j].x, feature2[j].y, win_sz.height, win_sz.width);
				feature[j] = feature2[j];
			}
			else
				fprintf(a, "not detect!\n");
		}
		cvCopyImage(g_cmp_frame,g_frame);
		cmp_frame = cvQueryFrame(src);
		cvCvtColor(cmp_frame, g_cmp_frame, CV_BGR2GRAY);
	}



	/*
	for (int i = 0; i < 7; i++)
		fprintf(a, "%d %d %d %d\n", (int)feature[i].x, (int)feature[i].y, win_sz.height, win_sz.width);
*/
	fclose(a);

	//cvCircle(frame, cvPoint(193, 258), 3, cvScalar(0xff, 0x00, 0x00));
	//cvShowImage("1-1", frame);
	//cvWaitKey(0);
	cvReleaseCapture(&src);
	//cvDestroyWindow("1-1");
}

//2
void Copencvhw4Dlg::OnBnClickedButton2()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CvCapture* src = cvCreateFileCapture("./HW_video.mp4");
	IplImage* frame = cvQueryFrame(src);
	cvNamedWindow("2", CV_WINDOW_AUTOSIZE);
	double f = cvGetCaptureProperty(src, 5);//get FPS of the video
	FILE *a = fopen("hw3_2.txt", "r");
	
	CvPoint2D32f *feature = new CvPoint2D32f[7];
	
	//printf("%f", f);
	while (1)
	{

		if (fscanf(a, "%s")!=EOF){
			for (int i = 0; i < 7; i++)
			{
				fscanf(a, "%f %f %d %d", &feature[i].x, &feature[i].y);
			}
			for (int i = 0; i < 7; i++)
			{

				cvLine(frame, cvPoint(feature[i].x - 5, feature[i].y - 5), cvPoint(feature[i].x + 5, feature[i].y - 5), cvScalar(0x00, 0x00, 0xff));
				cvLine(frame, cvPoint(feature[i].x - 5, feature[i].y), cvPoint(feature[i].x + 5, feature[i].y), cvScalar(0x00, 0x00, 0xff));
				cvLine(frame, cvPoint(feature[i].x - 5, feature[i].y + 5), cvPoint(feature[i].x + 5, feature[i].y + 5), cvScalar(0x00, 0x00, 0xff));
				cvLine(frame, cvPoint(feature[i].x - 5, feature[i].y - 5), cvPoint(feature[i].x - 5, feature[i].y + 5), cvScalar(0x00, 0x00, 0xff));
				cvLine(frame, cvPoint(feature[i].x, feature[i].y - 5), cvPoint(feature[i].x, feature[i].y + 5), cvScalar(0x00, 0x00, 0xff));
				cvLine(frame, cvPoint(feature[i].x + 5, feature[i].y - 5), cvPoint(feature[i].x + 5, feature[i].y + 5), cvScalar(0x00, 0x00, 0xff));
			}
		}
		cvShowImage("2", frame);
		frame = cvQueryFrame(src);
		if (cvWaitKey(1000 / f) == 27) break;
	}

	cvReleaseImage(&frame);
	cvReleaseCapture(&src);
	cvDestroyWindow("2");
}

//3
void Copencvhw4Dlg::OnBnClickedButton3()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CvCapture* src = cvCreateFileCapture("./HW_video.mp4");
	IplImage* frame = cvQueryFrame(src);
	IplImage* g_frame = cvCreateImage(cvGetSize(frame), IPL_DEPTH_8U, 1);
	cvCvtColor(frame, g_frame, CV_BGR2GRAY);
	//cvNamedWindow("1-1", CV_WINDOW_AUTOSIZE);
	CvPoint2D32f *feature = new CvPoint2D32f[7];
	CvPoint2D32f *feature2 = new CvPoint2D32f[7];
	feature[0] = cvPoint2D32f(117.0f, 72.0f);
	feature[1] = cvPoint2D32f(111.0f, 95.0f);
	feature[2] = cvPoint2D32f(118.0f, 168.0f);
	feature[3] = cvPoint2D32f(135.0f, 240.0f);
	feature[4] = cvPoint2D32f(129.0f, 260.0f);
	feature[5] = cvPoint2D32f(175.0f, 269.0f);
	feature[6] = cvPoint2D32f(193.0f, 257.0f);
	CvSize win_sz = cvSize(10, 10);

	IplImage* cmp_frame = cvQueryFrame(src);
	IplImage* g_cmp_frame = cvCreateImage(cvGetSize(cmp_frame), IPL_DEPTH_8U, 1);
	cvCvtColor(cmp_frame, g_cmp_frame, CV_BGR2GRAY);

	char features_found[7];
	float feature_errors[7];
	CvSize pyr_sz = cvSize(frame->width + 8, cmp_frame->height / 3);
	IplImage* pyrA = cvCreateImage(pyr_sz, IPL_DEPTH_8U, 1);
	IplImage* pyrB = cvCreateImage(pyr_sz, IPL_DEPTH_8U, 1);

	FILE *a = fopen("hw3_3.txt", "w");
	int f_cnt = cvGetCaptureProperty(src, CV_CAP_PROP_FRAME_COUNT);
	/*int f_cnt;
	scanf("%d", &f_cnt);*/
	printf("%d", f_cnt);
	for (int i = 0; i < f_cnt-2; i++)
	{
		cvCalcOpticalFlowPyrLK(
			g_frame,
			g_cmp_frame,
			pyrA,
			pyrB,
			feature,
			feature2,
			7,
			win_sz,
			5,
			features_found,
			feature_errors,
			cvTermCriteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 20, 0.3),
			0
			);

		fprintf(a, "frame%04d\n", i);
		for (int j = 0; j < 7; j++)
		{
			//fprintf(a, " %d", features_found[i]);
			if (features_found[j] != 0)
			{
				fprintf(a, "%f %f %d %d\n", feature2[j].x, feature2[j].y, win_sz.height, win_sz.width);
				feature[j] = feature2[j];
			}
			else
				fprintf(a, "not detect!\n");
		}
		cvCopyImage(g_cmp_frame, g_frame);
		cmp_frame = cvQueryFrame(src);
		cvCvtColor(cmp_frame, g_cmp_frame, CV_BGR2GRAY);
	}

	

	CvCapture* src2 = cvCreateFileCapture("./HW_video.mp4");
	frame = cvQueryFrame(src2);
	cvNamedWindow("3", CV_WINDOW_AUTOSIZE);
	double f = cvGetCaptureProperty(src2, 5);//get FPS of the video
	FILE *b = fopen("hw3_2.txt", "r");

	

	//printf("%f", f);
	while (1)
	{

		if (fscanf(b, "%s") != EOF){
			for (int i = 0; i < 7; i++)
			{
				fscanf(b, "%f %f %d %d", &feature[i].x, &feature[i].y);
			}
			for (int i = 0; i < 7; i++)
			{

				cvLine(frame, cvPoint(feature[i].x - 5, feature[i].y - 5), cvPoint(feature[i].x + 5, feature[i].y - 5), cvScalar(0x00, 0x00, 0xff));
				cvLine(frame, cvPoint(feature[i].x - 5, feature[i].y), cvPoint(feature[i].x + 5, feature[i].y), cvScalar(0x00, 0x00, 0xff));
				cvLine(frame, cvPoint(feature[i].x - 5, feature[i].y + 5), cvPoint(feature[i].x + 5, feature[i].y + 5), cvScalar(0x00, 0x00, 0xff));
				cvLine(frame, cvPoint(feature[i].x - 5, feature[i].y - 5), cvPoint(feature[i].x - 5, feature[i].y + 5), cvScalar(0x00, 0x00, 0xff));
				cvLine(frame, cvPoint(feature[i].x, feature[i].y - 5), cvPoint(feature[i].x, feature[i].y + 5), cvScalar(0x00, 0x00, 0xff));
				cvLine(frame, cvPoint(feature[i].x + 5, feature[i].y - 5), cvPoint(feature[i].x + 5, feature[i].y + 5), cvScalar(0x00, 0x00, 0xff));
			}
		}
		cvShowImage("3", frame);
		frame = cvQueryFrame(src2);
		if (cvWaitKey(1000 / f) == 27) break;
	}

	cvReleaseImage(&frame);
	cvReleaseCapture(&src);
	cvReleaseCapture(&src2);
	cvDestroyWindow("3");


	/*
	for (int i = 0; i < 7; i++)
	fprintf(a, "%d %d %d %d\n", (int)feature[i].x, (int)feature[i].y, win_sz.height, win_sz.width);
	*/
	fclose(a);
	fclose(b);
}
